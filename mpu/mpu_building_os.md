# MPU Building Recipe

- [MPU Building Recipe](#mpu-building-recipe)
  - [MPU Script](#mpu-script)
    - [Update OS, install basic apps, and install i3wm as an alternative window manager](#update-os-install-basic-apps-and-install-i3wm-as-an-alternative-window-manager)
    - [Add Metalab's MPA](#add-metalabs-mpa)
    - [install Apache Guacamole](#install-apache-guacamole)
    - [Install basic software](#install-basic-software)
    - [Install Jetson stats](#install-jetson-stats)
    - [Set Jack to start at boot](#set-jack-to-start-at-boot)
    - [Set Pure Data systemd service](#set-pure-data-systemd-service)
    - [Set SuperCollider systemd service](#set-supercollider-systemd-service)
    - [Set up i3wm](#set-up-i3wm)
    - [Adding a service to start JackTrip server](#adding-a-service-to-start-jacktrip-server)
    - [Adding a service to start JackTrip client](#adding-a-service-to-start-jacktrip-client)
    - [Make a systemd service for aj-snapshot](#make-a-systemd-service-for-aj-snapshot)
    - [Install Samba server](#install-samba-server)
    - [Make all systemd services available](#make-all-systemd-services-available)
    - [Mapping using jack in CLI](#mapping-using-jack-in-cli)
    - [Latency tests](#latency-tests)
    - [Jack available commands](#jack-available-commands)
    - [Install NoMachine](#install-nomachine)
    - [Ensure the NVIDIA is at the best power mode](#ensure-the-nvidia-is-at-the-best-power-mode)
    - [Install LivePose from source](#install-livepose-from-source)
    - [Finish and rebooting](#finish-and-rebooting)
  - [OPTIONAL: Installing a Wi-Fi dongle driver](#optional-installing-a-wi-fi-dongle-driver)
    - [Configure AP](#configure-ap)


This file contains the steps used to automatically generate an MPU image for NVIDIA Jetson boards.

## MPU Script

### Update OS, install basic apps, and install i3wm as an alternative window manager

```bash
sudo debconf-set-selections <<EOF
jackd2  jackd/tweak_rt_limits   boolean true
EOF
sudo DEBIAN_FRONTEND=noninteractive apt install -y jackd2
sudo apt update
sudo apt upgrade -y
sudo apt install -y i3 i3blocks htop vim feh rofi x11-utils git-lfs fonts-font-awesome jacktrip x11vnc gnome-tweaks
```

- Update desktop alternatives and select i3 as the default window manager:

```bash
sudo update-alternatives --install /usr/bin/x-session-manager x-session-manager /usr/bin/i3 60
echo "2" | sudo update-alternatives --config x-session-manager
sudo sed -i -e 's/^XSession=.*$/XSession=i3/'  /var/lib/AccountsService/users/${user_name}
```

### Add Metalab's MPA

```bash
sudo apt install -y coreutils software-properties-common wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-focal-arm64-jetson/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-focal-arm64-jetson/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update 
```

### install Apache Guacamole

- More info at https://guacamole.apache.org
- Reference: [Guacamole manual](https://guacamole.apache.org/doc/gug/)

- Install dependencies:

```bash
sudo apt install -y libcairo2-dev libpng-dev libjpeg-turbo8-dev libtool-bin libossp-uuid-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libwebsockets-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev jetty9 binutils make csh g++ sed gawk autoconf automake autotools-dev pip
```

- Clone git and install (set for version 1.4.0):

```bash
mkdir -p /sources && cd /sources
wget -qO guacamole-server-1.4.0.tar.gz "https://apache.org/dyn/closer.lua/guacamole/1.4.0/source/guacamole-server-1.4.0.tar.gz?action=download"
tar -xzf guacamole-server-1.4.0.tar.gz
cd guacamole-server-1.4.0
autoreconf -fi
./configure
make
sudo make install
sudo ldconfig
cd /sources
wget -qO guacamole.war "https://apache.org/dyn/closer.lua/guacamole/1.4.0/binary/guacamole-1.4.0.war?action=download"
sudo cp /sources/guacamole.war /var/lib/jetty9/webapps/guacamole.war
```

- Create the Guacamole home:

```bash
sudo mkdir /etc/guacamole
```

- Create `user-mapping.xml`

```bash
cat <<- "EOF" | sudo tee /etc/guacamole/user-mapping.xml
<user-mapping>
    <authorize
    username="mpu"
    password="mappings">
        <connection name="MPU">
        <protocol>vnc</protocol>
        <param name="hostname">localhost</param>
        <param name="port">5900</param>
        <param name="password">mappings</param>
        </connection>
    </authorize>
</user-mapping>
EOF
sudo sed -i -e "s/mappings/${passvar}/g" /etc/guacamole/user-mapping.xml
```

```bash
sudo mv /var/lib/jetty9/webapps/root /var/lib/jetty9/webapps/root-OLD
sudo mv /var/lib/jetty9/webapps/guacamole.war /var/lib/jetty9/webapps/root.war
```

- Change Guacamole login screen:

```bash
sudo mkdir -p /etc/guacamole/extensions
sudo cp /sources/MPU/mpu/mpu.jar /etc/guacamole/extensions/mpu.jar
```

- Configure addresses

```bash
sudo apt install -y apache2
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests
sudo a2enmod rewrite
sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.bak
```

```bash
cat <<- "EOF" | sudo tee /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>

    RewriteEngine on
    RewriteRule ^/webmapper$ /webmapper/ [R]

    ProxyRequests Off
    ProxyPreserveHost On

    ProxyPass /webmapper http://127.0.0.1:50000
    ProxyPassReverse /webmapper http://127.0.0.1:50000
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/

</VirtualHost>
EOF
```

```bash
sudo systemctl restart apache2
```

- Set guacd to start at boot

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/guacd.service
[Unit]
Description=Run Guacamole server
After=multi-user.target

[Service]
Type=idle
Restart=always
User=mpu
ExecStart=/usr/local/sbin/guacd -b 127.0.0.1 -f

[Install]
WantedBy=default.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable --now guacd.service
```

### Install basic software

- Installing SuperCollider, SC3-Plugins:

```bash
sudo apt install -y supercollider sc3-plugins libmapper python3-netifaces webmapper qjackctl puredata aj-snapshot splash-mapper ndi2shmdata && \
sudo apt install -y livepose
```

```bash
sudo ln -s /usr/bin/nditoshmdata /usr/bin/ndi2shmdata
```

- Installing SATIE:

```bash
cd /sources
git clone https://gitlab.com/sat-mtl/tools/satie/satie.git
echo "Quarks.install(\"SC-HOA\");Quarks.install(\"/sources/satie\")" | sclang
```

### Install Jetson stats

```bash
sudo -H pip3 install -U jetson-stats
```

### Set Jack to start at boot

- Add a dbus security policy:

```bash
cat <<- "EOF" | sudo tee /etc/dbus-1/system.conf
<policy user="micah">
     <allow own="org.freedesktop.ReserveDevice1.Audio0"/>
     <allow own="org.freedesktop.ReserveDevice1.Audio1"/>
</policy>
EOF
```

- Add path to environment:

```bash
cat <<- "EOF" | sudo tee /etc/environment
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket
EOF
```

- Create the systemd service file:

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/jackaudio.service
[Unit]
Description=JACK Audio
After=sound.target

[Service]
LimitRTPRIO=infinity
LimitMEMLOCK=infinity
User=mpu
Environment="JACK_NO_AUDIO_RESERVATION=1"
ExecStart=/usr/bin/jackd -R -P95 -t2000 -dalsa -dhw:1 -p256 -n2 -r48000 -s &

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
```

- Some commands:

- List information and connections on ports: `jack_lsp -c`
- Connect ports: `jack_connect [ -s | --server servername ] [-h | --help ] port1 port2` (The exit status is zero if successful, 1 otherwise)
- Disconnect ports: `jack_disconnect [ -s | --server servername ] [ -h | --help ] port1 port2`

### Set Pure Data systemd service

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/puredata.service
[Unit]
Description=Pure Data
After=sound.target jackaudio.service
ConditionPathExists=/home/${user_name}/Documents/default.pd

[Service]
LimitRTPRIO=infinity
LimitMEMLOCK=infinity
User=mpu
ExecStart=pd -nogui -noprefs -rt -jack -inchannels 2 -outchannels 2 /home/mpu/Documents/default.pd

[Install]
WantedBy=multi-user.target
EOF
sudo sed -i -e "s:/home/mpu/Documents:/home/${user_name}/Documents:" /lib/systemd/system/puredata.service
sudo systemctl daemon-reload
```

### Set SuperCollider systemd service

- OBS: starting as a user service to allow required access

```bash
mkdir -p /home/${user_name}/.config/systemd/user
cat <<- "EOF" | tee /home/${user_name}/.config/systemd/user/supercollider.service
[Unit]
Description=SuperCollider
After=multi-user.target
ConditionPathExists=/home/mpu/Documents/default.scd

[Service]
Type=idle
Restart=always
ExecStart=sclang -D /home/mpu/Documents/default.scd

[Install]
WantedBy=default.target
EOF
sed -i -e "s:/home/mpu/Documents:/home/${user_name}/Documents:" /home/${user_name}/.config/systemd/user/supercollider.service
sudo chmod 644 /home/${user_name}/.config/systemd/user/supercollider.service
systemctl --user daemon-reload
```

### Set up i3wm

- Copy i3_config to `/home/${user_name}/.config/i3` and rename to `config`:
- Copy i3status.conf to `/etc`:

```bash
mkdir -p /home/${user_name}/.vnc
cat <<- "EOF" | tee /home/${user_name}/.vnc/passwd
mappings
EOF
sed -i -e "s/mappings/${passvar}/" /home/${user_name}/.vnc/passwd
mkdir /home/${user_name}/.config/i3
cp /sources/MPU/mpu/i3_config /home/${user_name}/.config/i3/config
sudo cp /sources/MPU/mpu/i3status.conf /etc/i3status.conf
cp /sources/MPU/mpu/wallpaper.png /home/${user_name}/Pictures/wallpaper.png
sudo sed -i -e "s/MPU/${mpu_name}/" /etc/i3status.conf
```

- OBS: for checking Font Awesome: https://fontawesome.com/v5/cheatsheet

### Adding a service to start JackTrip server

- OBS: client name is the name of the other machine

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/jacktrip_server.service
[Unit]
Description=Run JackTrip server
After=multi-user.target

[Service]
Type=idle
Restart=always
ExecStart=/sources/jacktrip/builddir/jacktrip -s --clientname jacktrip_client

[Install]
WantedBy=default.target
EOF
```

- To enable the service at boot: `sudo systemctl enable jacktrip_server.service`

### Adding a service to start JackTrip client

- Replace the IP address for the server IP.

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/jacktrip_client.service
[Unit]
Description=Run JackTrip client
After=multi-user.target

[Service]
Type=idle
Restart=always
ExecStart=/sources/jacktrip/builddir/jacktrip -c 192.168.5.1 --clientname jacktrip_client

[Install]
WantedBy=default.target
EOF
```

- If you want to enable the client, disable the service and run `sudo systemctl enable jacktrip_client.service`
- To manually use as a client with IP address: `./jacktrip -c [xxx.xxx.xxx.xxx]`, or with name: `./jacktrip -c ${mpu_name}.local`

### Make a systemd service for aj-snapshot

- More info at [http://aj-snapshot.sourceforge.net/](http://aj-snapshot.sourceforge.net/)

- To create a snapshot: `aj-snapshot -f /home/${user_name}/Documents/default.connections`
- To remove all Jack connections: `aj-snapshot -xj`
- To save connections: `sudo aj-snapshot -f /home/${user_name}/Documents/default.connections`
- To restore connections: `sudo aj-snapshot -r /home/${user_name}/Documents/default.connections`

- Set custom Jack connections to load at boot:

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/ajsnapshot.service
[Unit]
Description=AJ-Snapshot
After=sound.target jackaudio.service
ConditionPathExists=/home/${user_name}/Documents/default.connections

[Service]
Type=idle
Restart=always
User=mpu
ExecStart=/usr/local/bin/aj-snapshot -d /home/${user_name}/Documents/default.connections

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
```

- If you want to save a ajsnapshot file run: `aj-snapshot -f /home/${user_name}/Documents/default.connections`

### Install Samba server

- OBS: Choose *No* if asked **Modify smb.conf to use WINS settings from DHCP?**

```bash
sudo apt install -y samba
sudo systemctl stop smbd.service
sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.orig
```

- Create Samba configuration file and set the user:

```bash
cat <<- "EOF" | sudo tee /etc/samba/smb.conf
[global]
        server string = MPUXXX
        server role = standalone server
        interfaces = lo eth0 wlan0
        bind interfaces only = no
        smb ports = 445
        log file = /var/log/samba/smb.log
        max log size = 10000
        map to guest = bad user

[Documents]
        path = "/home/mpu/Documents"
        read only = no
        browsable = yes
        valid users = mpu
        vfs objects = recycle
        recycle:repository = .recycle
        recycle:touch = Yes
        recycle:keeptree = Yes
        recycle:versions = Yes
        recycle:noversions = *.tmp,*.temp,*.o,*.obj,*.TMP,*.TEMP
        recycle:exclude = *.tmp,*.temp,*.o,*.obj,*.TMP,*.TEMP
        recycle:excludedir = /.recycle,/tmp,/temp,/TMP,/TEMP

EOF
sudo sed -i -e "s/MPUXXX/${mpu_name}/" /etc/samba/smb.conf
sudo sed -i -e "s:/home/mpu/Documents:/home/${user_name}/Documents:" /etc/samba/smb.conf
(echo ${passvar}; echo ${passvar}) | sudo smbpasswd -a ${user_name}
sudo smbpasswd -e ${user_name}
sudo systemctl enable --now smbd.service
```

### Make all systemd services available

```bash
sudo systemctl daemon-reload
```

### Mapping using jack in CLI

- Check available devices: `cat /proc/asound/cards`. If you have multiple devices available, can call them by name
- Lists jack available ports: `jack_lsp`
- List information and connections on ports: `jack_lsp -c` or `jack_lsp -A`
- Connect ports: `jack_connect [ -s | --server servername ] [-h | --help ] port1 port2` (The exit status is zero if successful, 1 otherwise)
- Disconnect ports: `jack_disconnect [ -s | --server servername ] [ -h | --help ] port1 port2`

### Latency tests

- Make sure JackTrip is running.

- Connect the necessary audio cable to create a loopback on the client's audio interface (audio OUT -> audio IN)
- For the loopback (same interface test): `jack_delay -I system:capture_2 -O system:playback_2`
- run the test: `jack_delay -O jacktrip_client.local:send_2 -I jacktrip_client.local:receive_2`

### Jack available commands

- To get a list on the computer, type **jack** and hit *Tab*

|command          |command              |command                     |command              |command                 |
|-----------------|---------------------|----------------------------|---------------------|------------------------|
| jack_alias      | jack_bufsize        | jack_capture               | jack_capture_gui    | jack_connect           |
| jackdbus        | jack_disconnect     | jack-dl                    | jack-dssi-host      | jack_evmon             |
| jack_load       | jack_lsp            | jack_metro                 | jack_midi_dump      | jack_midi_latency_test |
| jack_net_master | jack_net_slave      | jack_netsource             | jack-osc            | jack-play              |
| jack_samplerate | jack-scope          | jack_server_control        | jack_session_notify | jack_showtime          |
| jack_thru       | jack_transport      | jack-transport             | jack-udp            | jack_unload            |
| jack_control    | jack_cpu            | jack_cpu_load              | jackd               | jack_wait              |
| jack_freewheel  | jack_iodelay        | jack-keyboard              | jack_latent_client  | jack_midiseq           |
| jack_midisine   | jack_monitor_client | jack_multiple_metro        | jack-plumbing       |
| jack-rack       | jack_rec            | jack-record                | jack_test           |
| jack_simdtests  | jack_simple_client  | jack_simple_session_client | jack_zombie         |

- To check Jack logs: `sudo journalctl -u jack.service`

### Install NoMachine

- Instructions from [https://www.nomachine.com/getting-started-with-nomachine](https://www.nomachine.com/getting-started-with-nomachine)

```bash
mkdir -p /sources/NoMachine && cd /sources/NoMachine
wget https://download.nomachine.com/download/8.3/Arm/nomachine_8.3.1_1_arm64.deb
sudo dpkg -i nomachine_8.3.1_1_arm64.deb
```

### Ensure the NVIDIA is at the best power mode

```bash
sudo /usr/sbin/nvpmodel -m 8
```

### Install LivePose from source

- LivePose instructions extracted from [https://gitlab.com/sat-mtl/tools/livepose/-/blob/main/doc/Installation.md](https://gitlab.com/sat-mtl/tools/livepose/-/blob/main/doc/Installation.md):

```bash
sudo apt install coreutils gnupg2 wget sed software-properties-common
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-datasets/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list
cd /sources
git clone https://gitlab.com/sat-mtl/tools/livepose
cd /sources/livepose
sudo apt install -y git-lfs
git lfs fetch && git lfs pull
sudo apt update -y && \
sudo apt install -y \
    build-essential libpython3-dev python3 python3-pip python3-setuptools python3-venv python3-wheel \
    python3-cycler python3-dateutil python3-dev python3-filterpy python3-getch python3-imageio python3-lap \
    python3-matplotlib python3-networkx python3-numpy python3-scipy python3-six python3-skimage python3-tk \
    v4l-utils \
    python3-mediapipe \
    python3-mmcv python3-mmdetection python3-xtcocotools python3-mmpose \
    python3-opencv \
    python3-liblo \
    python3-libmapper \
    python3-librealsense2 \
    python3-torch python3-torchvision python3-torch2trt python3-trt-pose
sudo apt install -y librealsense2-udev-rules python3-librealsense2
python3 -m venv --system-site-packages --symlinks /home/${user_name}/livepose_venv
source /home/${user_name}/livepose_venv/bin/activate
export SETUPTOOLS_USE_DISTUTILS=stdlib
pip3 install .
deactivate
```

- LivePose installation done!

### Finish and rebooting

```bash
echo "Build done!"
echo
echo "rebooting..."
echo
sudo reboot
```

## OPTIONAL: Installing a Wi-Fi dongle driver

To create an access point we need a WiFi dongle (Jetson boards do not have built-in Wi-Fi). This is an example of how to install the driver for TP-Link AC600 Archer T2U Nano WiFi USB adapter.
For a different adapter, please check the manufacturer.

Plug the dongle and run the following commands

```bash
mkdir -p /sources && \
sudo apt install -y dkms git build-essential libelf-dev && \
cd /sources && \
git clone https://github.com/aircrack-ng/rtl8812au.git && \
cd /sources/rtl8812au && \
sudo make dkms_install
```

Unplug the TP-Link Archer T2U nano adapter and plug it again. The LED will start to blink. Verify if the driver is installed and loaded using the command `sudo dkms status`.

### Configure AP

```bash
sudo nmcli dev wifi hotspot ifname wlan0 ssid MPUXXX password "${passvar}"
sudo sed -i -e 's/autoconnect=false/autoconnect=true/' /etc/NetworkManager/system-connections/Hotspot.nmconnection
```
