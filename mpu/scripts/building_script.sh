#!/bin/bash


# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

echo
echo "┌──────────────────────┐"
echo "│ MPU Script Generator |"
echo "│ Edu Meneses - 2022   |"
echo "│ Metalab - SAT        |"
echo "│ IDMIL - CIRMMT       |"
echo "└──────────────────────┘"
echo

header='#!/bin/bash\
\
echo\
echo "┌───────────────────────────┐"\
echo "│ MPU Image Generator Script|"\
echo "│ Edu Meneses - 2022        |"\
echo "│ Metalab - SAT             |"\
echo "│ IDMIL - CIRMMT            |"\
echo "└───────────────────────────┘"\
echo\
\
# exit when any command fails\
set -e\
\
# keep track of the last executed command\
trap '"'"'last_command=$current_command; current_command=$BASH_COMMAND'"'"' DEBUG\
# echo an error message before exiting\
trap '"'"'echo "\\"${last_command}\\" command filed with exit code $?."'"'"' EXIT\
\
user_name=$(logname)\
mpu_name=$(hostname)\
\
#while true; do\
#    read -p "Do you wish to install LivePose from source? " yn\
#    case $yn in\
#        [Yy]* ) export install_livepose=yes; break;;\
#        [Nn]* ) break;;\
#        * ) echo "Please answer yes or no.";;\
#    esac\
#done\
\
read -sp '"'"'Insert password for samba, vnc, and AP: '"'"' passvar\
echo\
echo "This MPU'"'"'s hostname is ${mpu_name}"\
echo "and the current user name is ${user_name}"\
echo\
\
read -r -s -p $'"'"'Press enter to continue...\\n\\n'"'"''


read -r -s -p $'Press enter to continue...\n\n'

cp ../mpu_building_os.md building_script.tmp

sed -i                        \
    -e "1i ${header}"         \
    -e '1,/## MPU Script/d'   \
    -e '/^## OPTIONAL/,$d'    \
    -e '/```bash/d'           \
    -e '/```/d'               \
    -e 's/###/sudo -v\n#/'    \
    -e 's/^- .*/&"/'          \
    -e 's/^- /echo "/'        \
    -e 's/^|.*/&"/'           \
    -e 's/^|/echo "|/'        \
    -e 's/`/\\`/g'            \
    building_script.tmp

sed -i '/^# Install LivePose.*/a if [ $install_livepose ]; then' building_script.tmp
sed -i '/^.*LivePose installation done.*/a fi' building_script.tmp

mv building_script.tmp run_script.sh

echo "... making run_script.sh executable:"

sudo chmod +x run_script.sh

echo
echo "Done building the script."
echo "Execute the script (run_script.sh) to build the MPU."
echo

trap - EXIT